import { Component } from '@angular/core';
import { ChatroomService } from './chatroom.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  chatButtonClicked: boolean;
  userName: string;
  message: string;
  

  constructor(private chatRoomService: ChatroomService){}

  Chat(){
  	 let name = this.userName;
  	 document.getElementById("chats").innerText = "Welcome "+ name + "\n";
     this.chatRoomService.GetMessages().subscribe((data: string) => {
     for (var i = 0; i < data.split(',').length; i++){
     		document.getElementById("chats").innerText += data.split(',')[i].split(":")[0].split('"')[1] + ": " +  data.split(',')[i].split(":")[1].split('"')[1] + "\n";
     		
     	}
  });
     
}

Send(){
     let name = this.userName; 
     let message = this.message;
     this.chatRoomService.StoreMessages(message, name).subscribe((data: string) => {
     });
     document.getElementById("chats").innerText += name + ": " + message + "\n";
     
}
}
