import { Injectable } from '@angular/core';
import { Observable }  from 'rxjs';
import { Http, Response } from '@angular/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ChatroomService {

  constructor(private http: Http) { }
  data: string;
  private messageUrl: string;

  GetMessages() : Observable<string>
  {
  	 let getMessagesUrl = 'http://localhost:8080/chatroom/';

     return this.http.get(getMessagesUrl).pipe(map((res: any) => res._body));

  }  

  StoreMessages(message, user) : Observable<string>
  {
  	 let url = 'http://localhost:8080/chatroom/' + user + '/' + message;

     return this.http.get(url).pipe(map((res: any) => res._body));

  }  

  SendMessages(message, user): Observable<string>
  {
    let url = 'http://localhost:8080/chatroom/' + user + '/' + message;

    return this.http.get(url).pipe(map((res: any) => res._body));
  }
}
